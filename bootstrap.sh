#!/bin/bash


source "$(pwd)/conf/spinner.sh"

if [ "$(id -u)" != "0" ]; then
    echo "This script must be run as root to install software. Please re-run it as 'sudo ./bootstrap.sh'" 1>&2
    exit 1
fi

HOSTS=/etc/hosts
HOST="local.feadback.usic.at"
LOG=$(pwd)/bootstrap.log
CONF=$(pwd)/conf/
NGINX_CONF=$CONF$HOST".conf"
FPM_CONF=$CONF"www.conf"

start_spinner "Check if nginx and php5-fpm already installed"
dpkg -s nginx php5-fpm > /dev/null 2>&1
stop_spinner $?

if [ $? != "0" ]; then
    start_spinner "Installing web-server nginx and php-fpm"
    sudo apt-get update 2>&1 > $LOG && sudo apt-get install -f nginx php5-fpm  php5-gd php5-imagick php5-cli php5-common php5-curl> $LOG 2>&1
    stop_spinner $?
fi

start_spinner "Add host to hosts file"
sleep 2
sudo grep -q $HOST $HOSTS || sudo echo "127.0.0.1 $HOST" >> $HOSTS
stop_spinner $?


start_spinner "Copy config files"
sleep 2
cp $NGINX_CONF /etc/nginx/conf.d/ && cp $FPM_CONF /etc/php5/fpm/pool.d/
stop_spinner $?


start_spinner "Create assets and runtime directories"
sleep 2
mkdir -p {web/assets,protected/runtime} && chmod 777 {web/assets,protected/runtime} -R
stop_spinner $?

which composer > $LOG 2>&1 # check if composer already installed
if [ $? != "0" ]; then
    start_spinner "Getting composer (PHP dependens manager)"
    curl -sS https://getcomposer.org/installer | php > $LOG 2>&1 && sudo mv composer.phar /usr/local/bin/composer
    stop_spinner $?
fi

start_spinner "Install all dependenses"
composer install > $LOG 2>&1
stop_spinner $?

start_spinner "Restart php5-fpm"
sudo /etc/init.d/php5-fpm restart > $LOG 2>&1
stop_spinner $?

start_spinner "Restart nginx"
sudo /etc/init.d/nginx restart > $LOG 2>&1
stop_spinner $?

