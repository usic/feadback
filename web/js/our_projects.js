$(document).ready(function()
{
	$("#send").click(
    function(){

    	$(".control-group, #send, #subheader").css('display','none');
    	$(".after_form").css('display','block');

    	$("#cat").hover(
			function () {
        		$("#cat img").css('opacity','0.8');
        		$("#cat p").css('opacity','0.8');
      		},
			function(){
        		$("#cat img").css('opacity','1');
        		$("#cat p").css('opacity','1');
      		}
  		);

  		$("#cat").click(
    		function(){
        		$("#wiki").animate({
            		opacity:'1',
            		top:'0',
            		marginTop:'0'
          		},500);
          		$("#tt").animate({
            		opacity:'1',
            		top:'65%',
            		marginTop:'0',
            		left:'100%',
            		marginLeft:'-55px'
          		},1000);
          		$("#fs").animate({
            		opacity:'1',
            		top:'65%',
            		marginTop:'0',
            		left:'0',
            		marginLeft:'0'
          		},1500);

        		$("#projects_list").css('display','block');

        		$("#cat").hover(
					function () {
        				$("#cat img").css('opacity','0.8');
        				$("#cat p").css('display','none');
        				$("#main").css('display','block');
        				$("#main").css('opacity','1');
      				},
					function(){
        				$("#cat img").css('opacity','1');
        				$("#main").css('display','none');
        				$("#our_projects").css('display','block');
      				}
  				);

        		$("#wiki").hover(
					function () {
          				$("#wiki").css('opacity','0.7');
        				$("#cat p").css('display','none');
        				$("#cat #c_wiki").css('display','inline-block');
      				},
					function(){
   						$("#wiki").css('opacity','1');
        				$("#cat p").css('display','none');
        				$("#cat #our_projects").css('display','inline-block');
      				}
    			);

    			$("#fs").hover(
					function () {
        				$("#fs").css('opacity','0.7');
        				$("#cat p").css('display','none');
        				$("#cat #c_fs").css('display','inline-block');
      				},
					function(){
          				$("#fs").css('opacity','1');
        				$("#cat p").css('display','none');
        				$("#cat #our_projects").css('display','inline-block');
      				}
    			);

    			$("#tt").hover(
					function () {
        				$("#tt").css('opacity','0.7');
        				$("#cat p").css('display','none');
        				$("#cat #c_tt").css('display','inline-block');
      				},
					function(){
         			 	$("#tt").css('opacity','1');
        				$("#cat p").css('display','none');
        				$("#cat #our_projects").css('display','inline-block');
      				}
    			);
			});

	$("#cat").trigger("click");
    });	

});