<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<link rel="stylesheet" type="text/css" href="/css/feedback.css">

	<title>Feadback for UKMA Student Internet Center</title>
	<script type="text/javascript" src="/js/our_projects.js"></script>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="span8 offset2">
        <?php echo $content; ?>
    </div>
</div>
</body>
</html>
