<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */
?>



<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>
<header>
	<h2><a href="http://usic.at">USIC</a>feedback</h2>
</header>
<div id="subheader">
	<h3>Нам дуже важливі ваші відгуки для<br> покращення роботи сервісів</h3>
</div>

<div class="form">

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'ContactForm',
    'type'=>'horizontal',
    'enableAjaxValidation'=>true,
    'htmlOptions'=>array('class'=>'well'),
    'focus'=>array($model,'body'),
    'clientOptions'=>array(
        'validationDelay'=>500,
        'validateOnType'=>false,
        'validateOnSubmit'=>true,
        'afterValidateAttribute'=>"js:function(form, attribute, data, hasError) {
            if(attribute['id'].match(/verifyCode$/) && hasError){
                $('#captcha a').trigger('click');
            }
        }",
        'validateOnChange'=>true,
        'afterValidate'=>"js:function(form, data, hasError) {
            if(!hasError){
                alert(data.message);
                if(history.length!=0)
                    history.back();
            }
        }",
    )
                                                                   )); ?>

		<?php echo $form->textAreaRow($model,'body',array('rows'=>8, 'cols'=>70,'class'=>'input-xlarge')); ?>

        <?php if(Yii::app()->user->isGuest):?>
            <?php echo $form->textFieldRow($model,'email',array('class'=>'input-xlarge')); ?>
        <?php endif;?>

        <?php echo $form->dropDownListRow($model, 'subject', CHtml::listData(
                ContactForm::getSubjects(),'id','text'),
            array(
                 'empty' => 'Оберіть тему',
                 'class'=>'input-xlarge'
            ));?>


    <?php if(CCaptcha::checkRequirements() && Yii::app()->user->isGuest): ?>
        <div class="control-group">
            <label class="control-label" for="ContactForm_verifyCode"></label>
            <div class="controls" id="captcha">
                <?php #$this->widget('CCaptcha'); ?>
                <?php $this->widget('CCaptcha', array(
                                                     'clickableImage'=>true,
                                                     'showRefreshButton'=>true,
                                                     'buttonLabel'=>'Оновити',
                                                     'imageOptions'=>array(
                                                        'style'=>'border:none;',
                                                        'alt'=>'Зображення з кодом активації',
                                                        'title'=>'Для оновлення зображення клікніть по ньому'
                                                     )
                                                )
                ); ?>

            </div>
            <?php echo $form->textFieldRow($model,'verifyCode'); ?>
            </div>
        <?php endif;?>
        <div id="send">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Надіслати')); ?>
        </div>

        <div class="after_form">
            <h3>Дякуємо, що приділили нам час. Залишайтеся з нами.</h3>
            <section class="projects">
                <div id="wrapper">
                    <div id="cat">
                        <a href="http://usic.at"><img src="/img/Usic_logo.svg" alt="Котик"></a>
                        <p id="our_projects">Наші проекти</p>
                        <p id="main">Головна</p>
                        <p id="c_wiki">USIC Wiki</p>
                        <p id="c_fs">Fileshare</p>
                        <p id="c_tt">Time table</p>
                    </div>
                    <div id="projects_list">
                        <ul>
                            <a href="http://wiki.usic.org.ua"><li id="wiki"></li></a>
                            <a href="http://tt.usic.at"><li id="tt"></li></a>
                            <a href="http://fs.usic.at"><li id="fs"></li></a>
                        </ul>
                    </div>
                </div>  
            </section>
        </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>
