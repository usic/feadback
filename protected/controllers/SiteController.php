<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xf5f5f5,
                'transparent'=>true,
                'foreColor'=>0x109DF4, //цвет символов
                'testLimit'=>2
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
        Yii::log('Begin');
        if(isset($_POST['ajax']) && $_POST['ajax']==='ContactForm')
        {
            $result = CActiveForm::validate($model);
            if($result == '[]'){
                if($this->sendMail($model))
                    echo CJSON::encode(array('message'=>ContactForm::SUCCESS));
                else
                    echo CJSON::encode(array('message'=>ContactForm::ERROR));
                Yii::app()->end();
            }
            echo $result;
            Yii::app()->end();
        }
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
                if($this->sendMail($model))
                    Yii::app()->user->setFlash('contact',ContactForm::SUCCESS);
                else
                    Yii::app()->user->setFlash('contact',ContactForm::ERROR);
                $this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}
    private function sendMail($model)
    {
        $name='=?UTF-8?B?'.base64_encode($model->name).'?=';
        $subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
        $headers="From: $name <{$model->email}>\r\n".
            "Reply-To: {$model->email}\r\n".
            "MIME-Version: 1.0\r\n".
            "Content-type: text/plain; charset=UTF-8";
        Yii::log('send mail');
        #return mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
        return true;
    }
    
}
