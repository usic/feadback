<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel
{
	public $name;
	public $email;
	public $subject;
	public $body;
	public $verifyCode;

    const SUCCESS = 'Дякуємо, що приділили нам час. Залишайтеся з нами.',
          ERROR   = 'Вибачте, але не вдалося відправити Ваш відгук. Спробуйте пізніше.';
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('email, body', 'required','message'=>'{attribute} не може бути порожнім'),
            array('subject', 'required','message'=>'{attribute} не може бути порожньою'),
			// email has to be a valid email address
			array('email', 'email','checkMX'=>false,'message'=>'Введіть коректну адресу'),
            array('name','safe'),
            array('body','length','max'=>1000,'min'=>10,'tooShort'=>'Скажіть більш розгорнуто','tooLong'=>'Скажіть конкретніше'),
			// verifyCode needs to be entered correctly
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements() || !Yii::app()->user->isGuest,
                  'message'=>'Код перевірки не коректний'),
		);
	}

    public function beforeValidate()
    {
        $this->name = Yii::app()->user->name;
        if(!Yii::app()->user->isGuest)
            $this->email = Yii::app()->user->email;
        return parent::beforeValidate();
    }

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>'Код перевірки',
            'email'     =>'E-mail',
            'subject'   =>'Тема',
            'body'      =>'Відгук'
		);
	}

    public function getSubjects()
    {
        return array(
            array('id'=>'thx','text'=>'Подяка'),
            array('id'=>'staff','text'=>'Стажування'),
            array('id'=>'projects','text'=>'Участь у проектах'),
            array('id'=>'bug','text'=>'Проблема у роботі з сервісами'),
            array('id'=>'it_wish','text'=>'Пропозиція по розробці проектів'),
            array('id'=>'wish','text'=>'Побажання з приводу роботи Центру'),
            array('id'=>'center','text'=>'Проблема при роботі в приміщенні Центру'),
        );
    }
}
