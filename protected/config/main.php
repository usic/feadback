<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Feadback',
    'id' => 'usic',
    'defaultController'=>'site/contact',
	// preloading 'log' component
	'preload'=>array('log','bootstrap'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(

	),

	// application components
	'components'=>array(
        'request' => array(
            'enableCsrfValidation' => true,
            'enableCookieValidation'=>true,
        ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
//            'class' => 'MyWebUser', 
		),
/*        'redisCache' => array(
            'class' => 'application.lib.redis.CRedisCache',
            'predisPath' => 'application.lib.redis.Predis',
            'servers' => array(
                array(
                    'database' => 0,
                    'host' => '127.0.0.1',
                    'port' => 6379,
                ),
            ),
        ),
        'session' => array(
            //uncomment this to store session in redis
            'class' => 'CCacheHttpSession',
            'cacheID' => 'redisCache',
            'autoStart' => false,
            'cookieMode' => 'allow',
            'cookieParams' => array(
                'path' => '/',
                'domain' => '.usic.at',
                'httpOnly' => true,
            )
        ),*/
        'bootstrap' => array(
            'class' => 'application.lib.clevertech.yii-booster.src.components.Bootstrap',
            'responsiveCss' => true,
        ),

		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning,info',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),
	'params'=>array(
		'adminEmail'=>'webmaster@example.com',
	),
);
